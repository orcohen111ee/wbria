import React, { useMemo } from "react";
import { INDEXEDDB_KEY_AUDIO_OUTPUT, isDesktopApp } from "../../../const";
import { useAppRoot } from "../../../001_provider/001_AppRootProvider";
import { useAppState } from "../../../001_provider/001_AppStateProvider";
import { useIndexedDB } from "@dannadori/voice-changer-client-js";
import { useMessageBuilder } from "../../../hooks/useMessageBuilder";

export type HeaderAreaProps = {
    mainTitle: string;
    subTitle: string;
};

export const HeaderArea = (props: HeaderAreaProps) => {
    const { appGuiSettingState } = useAppRoot();
    const messageBuilderState = useMessageBuilder();
    const { clearSetting } = useAppState();

    const { removeItem } = useIndexedDB({ clientType: null });

    useMemo(() => {
        messageBuilderState.setMessage(__filename, "github", { ja: "github", en: "github" });
        messageBuilderState.setMessage(__filename, "manual", { ja: "マニュアル", en: "manual" });
        messageBuilderState.setMessage(__filename, "screenCapture", { ja: "録画ツール", en: "Record Screen" });
        messageBuilderState.setMessage(__filename, "support", { ja: "支援", en: "Donation" });
    }, []);

    const peterpanIcon = useMemo(() => {
        return (
            <span className="link tooltip">
                <img src="./assets/icons/peter.jfif" width="36px" height="58px" />
            </span>
        );
    }, []);

    const piedPiperLogo = useMemo(() => {
        return (
            <span className="link tooltip">
                <img src="./assets/icons/hat-removebg-preview.png" width="36px" height="36px" />
            </span>
        );
    }, []);

    





    const headerArea = useMemo(() => {
        const onClearSettingClicked = async () => {
            await clearSetting();
            await removeItem(INDEXEDDB_KEY_AUDIO_OUTPUT);
            location.reload();
        };

        return (
            <div className="headerArea">
                <div className="title1">
                    <div className="icons">
                        <span className="belongings">{peterpanIcon}</span>
                       
                    </div>
                    <span className="title">{props.mainTitle}</span>
                    <span className="belongings">{piedPiperLogo}</span>
                    <span className="title-version">{props.subTitle}</span>
                </div>

                <div className="icons">
                    <span className="belongings">
                        <button className="btn" type="button" onClick={onClearSettingClicked}>
                            <strong>Restore Defaults</strong>
                            <div id="glow">
                                <div className="circle"></div>
                                <div className="circle"></div>
                            </div>
                        </button>
                       
                        {/* <div className="belongings-button" onClick={onReloadClicked}>reload</div>
                        <div className="belongings-button" onClick={onReselectVCClicked}>select vc</div> */}
                    </span>
                </div>
            </div>
        );
    }, [props.subTitle, props.mainTitle, appGuiSettingState.version, appGuiSettingState.edition]);

    return headerArea;
};
